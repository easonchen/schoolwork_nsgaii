clear;clc;close all;
x=-5:0.001:10;
load('VarName1.mat');
load('VarName2.mat');
load('VarName3.mat');
load('VarName4.mat');
load('VarName5.mat');

% 目標函數一
for n=1:length(x)
    if x(n)<=1
        y1(n)=-x(n);
    elseif x(n)>1 && x(n)<=3
        y1(n)=x(n)-2;
    elseif x(n)>3 && x(n)<=4
        y1(n)=4-x(n);
    else
        y1(n)=x(n)-4;
    end
end

%  目標函數二
y2=(x-5).^2;

% 將目標函數一、二的值平方相加開根號求最小值找最佳解
[obj c]=min(sqrt(y1.^2+y2.^2));
val=x(c) % 最佳解

% 目標函數一圖
figure;
plot(x,y1,'.')
title('x=-5~10');xlabel('f1(x)');ylabel('f2(x)');
grid on;set(gca,'fontsize',20);

% 目標函數二圖
figure;
plot(x,y2,'.')
title('x=-5~10');xlabel('f1(x)');ylabel('f2(x)');
grid on;set(gca,'fontsize',20);

% 目標函數值一、二之二維圖
figure;
plot(y1,y2,'*')
title('x=-5~10');xlabel('f1(x)');ylabel('f2(x)');
grid on;set(gca,'fontsize',20);

% C++所計算出之"plot.out"的Preto Front圖
figure;
for n=1:5
m=int2str(n);
chr=['plot' m '.out'];
data=load(chr);
plot(data(:,1),data(:,2),'*');hold on;
title('Feasible and Non-dominated Objective Vector');xlabel('f1(x)');ylabel('f2(x)');
grid on;set(gca,'fontsize',20);
axis([min(data(:,1))-0.1 max(data(:,1))+0.1 min(data(:,2))-0.1 max(data(:,2))+0.1]);
end

% 最後一代群體的解，並求它分成兩邊的解平均。
figure;
for n=1:5
m=int2str(n);
chr2=['VarName' m];
% data2=str2double(chr2);
plot(1:length(eval(chr2)),eval(chr2),'*');hold on;
title('Feasible Variable_vectors for non-dominated solutions at last generation');xlabel('Population');ylabel('Solution');
grid on;set(gca,'fontsize',20);
% axis([min(data(:,1))-0.1 max(data(:,1))+0.1 min(data(:,2))-0.1 max(data(:,2))+0.1]);

i=1;j=1;solution=eval(chr2);
for p=1:length(solution)
    if solution(p)>mean(solution)
        solution_up(i)=solution(p);
        i=i+1;
    else
        solution_down(j)=solution(p);
        j=j+1;
    end
    end
sol1(n)=mean(solution_up(find(solution_up~=0)))
sol2(n)=mean(solution_down(find(solution_down~=0)))
end
sol1_mean=mean(sol1)
sol2_mean=mean(sol2)